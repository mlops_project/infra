import logging
import pathlib
import dotenv
import os
import asyncio
import httpx
import urllib
import mlflow
from mlflow.entities import ViewType
from logging import FileHandler, getLogger, INFO


BASE_DIR = pathlib.Path(__file__).parent.resolve()
ENV_VARS = dotenv.dotenv_values(BASE_DIR / ".env")


class Config:
    GITLAB_URL = "https://gitlab.com/api/v4/projects/46572294"
    TRIGGER_TOKEN = os.getenv("GITLAB_TRIGGER_TOKEN") or ENV_VARS["GITLAB_TRIGGER_TOKEN"]
    PRIVATE_TOKEN = os.getenv("GITLAB_PRIVATE_TOKEN") or ENV_VARS["GITLAB_PRIVATE_TOKEN"]

    MLFLOW_ADDRES = f"http://{os.getenv('SERVER_ADDRES') or ENV_VARS['SERVER_ADDRES']}/mlflow"
    MLFLOW_KEYS = [
        "AWS_ACCESS_KEY_ID",
        "AWS_SECRET_ACCESS_KEY",
        "MLFLOW_S3_ENDPOINT_URL",
        "ARTIFACT_ROOT",
    ]
    MLFLOW_S3 = os.getenv("MLFLOW_S3_ENDPOINT_URL") or ENV_VARS["MLFLOW_S3_ENDPOINT_URL"]
    MODEL_ENV_NAME = os.getenv("GITLAB_ENV_VAR_OVER_MODE") or ENV_VARS["GITLAB_ENV_VAR_OVER_MODE"]

    SLEEP_TIME = int(os.getenv("SLEEP_TIME") or ENV_VARS.get("SLEEP_TIME"))



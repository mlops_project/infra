import logging
import os
import asyncio
import httpx
import urllib
import mlflow
from mlflow.entities import ViewType
from logging import FileHandler, StreamHandler, getLogger, INFO
from config import Config, ENV_VARS
from datetime import datetime

logging.getLogger("httpx").setLevel("ERROR")
logger = getLogger("root")
logger.addHandler(FileHandler(filename="log.log"))
logger.addHandler(StreamHandler())
logger.setLevel(INFO)


def init_args():
	for key in Config.MLFLOW_KEYS:
		value = os.getenv(key)
		if value is None:
			value = ENV_VARS[key]
			os.environ[key] = value


def get_run_id(run_id: str) -> str:
	init_args()
	mlflow.set_tracking_uri(Config.MLFLOW_ADDRES)
	client = mlflow.client.MlflowClient(
		tracking_uri=Config.MLFLOW_ADDRES, registry_uri=Config.MLFLOW_S3
	)
	all_experiments = [exp.experiment_id for exp in mlflow.search_experiments()]
	try:
		run = client.search_runs(
			experiment_ids=all_experiments,
			filter_string=f"attributes.run_id = '{run_id}'",
			run_view_type=ViewType.ACTIVE_ONLY,
			max_results=1,
			# order_by=["metrics.accuracy_score_X_test DESC"],
		)[0]
	except Exception:
		return None
	else:
		return run


async def get_tags(client: httpx.AsyncClient) -> list[dict[str, str | datetime]] | None:
	r = await client.get(url="/repository/tags")
	tags_data: list[dict[str, str | datetime]] = []
	if r.status_code != 200:
		logger.info("gitlab api return error")
		return
	else:
		for i in r.json():
			data = {
				"name": i.get("name"),
				"date": datetime.fromisoformat(
					i.get("commit").get("committed_date")
				)
			}
			tags_data.append(data.copy())
		return tags_data


async def create_tag(client: httpx.AsyncClient, tag: str) -> None:
	await client.post(
		url=f"/repository/tags?tag_name={tag}&ref=main&message=auto_tag_creation"
	)


def get_latest_tag(tags: list[dict[str, str | datetime]]) -> str:
	latest = max([i["date"] for i in tags])
	for i in tags:
		if i["date"] == latest:
			return i["name"]


async def set_gitlav_env(client: httpx.AsyncClient, run_id: str):
	r = await client.put(
		url=f"/variables/{Config.MODEL_ENV_NAME}",
		json={
			"variable_type": "env_var",
			"key": Config.MODEL_ENV_NAME,
			"value": run_id,
			"protected": False,
			"masked": False,
			"raw": False,
			"environment_scope": "*"
		}
	)


async def trigger_pipeline():
	async with httpx.AsyncClient(base_url=Config.GITLAB_URL) as client:
		r = await client.post(url=f"/ref/main/trigger/pipeline?token={Config.TRIGGER_TOKEN}")


async def main():
	http_client = httpx.AsyncClient(
		base_url=Config.GITLAB_URL,
		headers={"PRIVATE-TOKEN": Config.PRIVATE_TOKEN}
	)
	tags = await get_tags(http_client)
	latest_tag_name = get_latest_tag(tags)
	while True:
		logger.info(f"[mlflow]:[INFO]: start new cycle: {Config.SLEEP_TIME}")
		tags = await get_tags(http_client)
		new_latest_tag_name = get_latest_tag(tags)
		logger.info(f"[mlflow]:[INFO]: tags: previous run id: {latest_tag_name}, new run id: {new_latest_tag_name}")
		if new_latest_tag_name != latest_tag_name:
			run = get_run_id(new_latest_tag_name)
			if run is not None:
				logger.info(f"[mlflow]:[INFO]: got new tags: {latest_tag_name}, new run id: {new_latest_tag_name}")
				await set_gitlav_env(http_client, new_latest_tag_name)
				await trigger_pipeline()
				latest_tag_name = new_latest_tag_name

		await asyncio.sleep(Config.SLEEP_TIME)


if __name__ == '__main__':
	asyncio.run(main())

import logging
import pathlib
import dotenv
import os
import asyncio
import httpx
import urllib
import mlflow
from mlflow.entities import ViewType
from logging import FileHandler, getLogger, INFO


base_dir = pathlib.Path(__file__).parent.parent.resolve()
env_vars = dotenv.dotenv_values(base_dir / ".env")

trigger_token = os.getenv("GITLAB_TRIGGER_TOKEN") or env_vars["GITLAB_TRIGGER_TOKEN"]
private_token = os.getenv("GITLAB_PRIVATE_TOKEN") or env_vars["GITLAB_PRIVATE_TOKEN"]
ref = urllib.parse.quote("feature/ci-cd", safe='/',)

gitlab_url = "https://gitlab.com/api/v4/projects/46572294"

mlflow_addres = f"http://{os.getenv('SERVER_ADDRES') or env_vars['SERVER_ADDRES']}/mlflow"
mflow_s3 = os.getenv("MLFLOW_S3_ENDPOINT_URL") or env_vars["MLFLOW_S3_ENDPOINT_URL"]
model_env_var_name = os.getenv("GITLAB_ENV_VAR_OVER_MODE") or env_vars["GITLAB_ENV_VAR_OVER_MODE"]

sleep_time = os.getenv("SLEEP_TIME") or env_vars.get("SLEEP_TIME")
sleep_time = int(sleep_time)

os_keys = [
	"AWS_ACCESS_KEY_ID",
	"AWS_SECRET_ACCESS_KEY",
	"MLFLOW_S3_ENDPOINT_URL",
	"ARTIFACT_ROOT",
]



def init_args():
	for key in os_keys:
		value = os.getenv(key)
		if value is None:
			value = env_vars[key]
			os.environ[key] = value


def get_latest_run_id() -> str:
	mlflow.set_tracking_uri(mlflow_addres)
	client = mlflow.client.MlflowClient(
		mlflow_addres,
		mflow_s3
	)
	all_experiments = [exp.experiment_id for exp in mlflow.search_experiments()]
	run = client.search_runs(
		experiment_ids=all_experiments,
		filter_string="",
		run_view_type=ViewType.ACTIVE_ONLY,
		max_results=1,
		order_by=["metrics.accuracy_score_X_test DESC"],
	)[0]
	run_id = run.info.run_id
	return run_id


async def trigger_pipeline():
	async with httpx.AsyncClient(base_url=gitlab_url) as client:
		r = await client.post(url=f"/ref/{ref}/trigger/pipeline?token={trigger_token}")


async def set_gitlav_env(run_id: str):
	async with httpx.AsyncClient(base_url=gitlab_url) as client:
		r = await client.put(
			url=f"/variables/{model_env_var_name}",
			headers={"PRIVATE-TOKEN": private_token},
			json={
				"variable_type": "env_var",
				"key": model_env_var_name,
				"value": run_id,
				"protected": False,
				"masked": False,
				"raw": False,
				"environment_scope": "*"
			}
		)


async def main():
	init_args()
	logging.getLogger("httpx").setLevel("ERROR")
	logger = getLogger("root")
	logger.setLevel(INFO)
	logger.addHandler(FileHandler(filename="log.log"))

	run_id = get_latest_run_id()
	print(run_id)
	while True:
		logger.info(f"[mlflow]:[INFO]: start new cycle: {sleep_time}")
		new_run_id = get_latest_run_id()
		if new_run_id != run_id:
			logger.info(f"[mlflow]:[INFO]: previous run id: {run_id}, new run id: {new_run_id}")
			await set_gitlav_env(new_run_id)
			await trigger_pipeline()
			run_id = new_run_id

		await asyncio.sleep(sleep_time)


if __name__ == '__main__':
	asyncio.run(main())


import mlflow.sklearn
import os
import mlflow
from mlflow.entities import ViewType
import numpy as np

def model():
    os.environ["AWS_ACCESS_KEY_ID"] = "YCAJE2FLNfw7QmIAn2n4tPXkr"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "YCNDXoIGWr7pzWkBkKvx3PGKIsBpOYraswRVLRKG"
    os.environ["MLFLOW_S3_ENDPOINT_URL"] = "https://storage.yandexcloud.net"
    os.environ["ARTIFACT_ROOT"]="s3://dvcbucket"
    mlflow.set_tracking_uri("http://188.227.32.199/mlflow")
    client = mlflow.client.MlflowClient("http://188.227.32.199/mlflow",
                                        "https://storage.yandexcloud.net/")
    all_experiments = [exp.experiment_id for exp in mlflow.search_experiments()]
    run = client.search_runs(
        experiment_ids=all_experiments,
        filter_string="",
        run_view_type=ViewType.ACTIVE_ONLY,
        max_results=1,
        order_by=["metrics.accuracy_score_X_test DESC"],
    )[0]
    run_id = run.info.run_id
    loaded_model = mlflow.sklearn.load_model("runs:/" + run_id + "/model")
    return loaded_model

if __name__ == "__main__":
    model()